package com.gitlab.wesleyosantos91.backend.abstractFactory.service.factory.impl;

import com.gitlab.wesleyosantos91.backend.abstractFactory.enumeration.TipoAnimal;
import com.gitlab.wesleyosantos91.backend.abstractFactory.service.Animal;
import com.gitlab.wesleyosantos91.backend.abstractFactory.service.factory.AbstractFactory;
import com.gitlab.wesleyosantos91.backend.abstractFactory.service.impl.Cachorro;
import com.gitlab.wesleyosantos91.backend.abstractFactory.service.impl.Pato;

public class AnimalFactory implements AbstractFactory<Animal> {

    @Override
    public Animal create(TipoAnimal tipo) {

        switch (tipo) {
            case PATO:
                return new Pato();
            case CACHORRO:
                return new Cachorro();
            default:
                throw new IllegalArgumentException("Animal não definido");
        }
    }
}
