package com.gitlab.wesleyosantos91.backend.abstractFactory.service.factory;

import com.gitlab.wesleyosantos91.backend.abstractFactory.enumeration.TipoAnimal;

public interface AbstractFactory<T> {
    T create(TipoAnimal tipo) ;
}