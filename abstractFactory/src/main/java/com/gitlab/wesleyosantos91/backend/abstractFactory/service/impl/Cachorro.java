package com.gitlab.wesleyosantos91.backend.abstractFactory.service.impl;

import com.gitlab.wesleyosantos91.backend.abstractFactory.service.Animal;

public class Cachorro implements Animal {

    @Override
    public String getAnimal() {
        return "Cachorro";
    }

    @Override
    public String getSom() {
        return "Au Au Au";
    }
}
