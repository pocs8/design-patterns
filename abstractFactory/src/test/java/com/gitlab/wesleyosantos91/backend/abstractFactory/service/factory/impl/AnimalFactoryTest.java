package com.gitlab.wesleyosantos91.backend.abstractFactory.service.factory.impl;

import com.gitlab.wesleyosantos91.backend.abstractFactory.enumeration.TipoAnimal;
import com.gitlab.wesleyosantos91.backend.abstractFactory.service.Animal;
import com.gitlab.wesleyosantos91.backend.abstractFactory.service.impl.Cachorro;
import com.gitlab.wesleyosantos91.backend.abstractFactory.service.impl.Pato;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnimalFactoryTest {

    private AnimalFactory factory;

    @BeforeEach
    void setUp() {
        factory = new AnimalFactory();
    }

    @Test
    @DisplayName("Deve criar uma instancia de Cachoroo")
    void deveCriarUmaInstanciaDeCachorro() {
        Animal animal = factory.create(TipoAnimal.CACHORRO);
        assertAll(() -> assertTrue(animal instanceof Cachorro),
                () -> assertEquals(animal.getAnimal(), "Cachorro"),
                () -> assertEquals(animal.getSom(), "Au Au Au"));
    }

    @Test
    @DisplayName("Deve criar uma instancia de Pato")
    void deveCriarUmaInstanciaDePato() {
        Animal animal = factory.create(TipoAnimal.PATO);
        assertAll(() -> assertTrue(animal instanceof Pato),
                () -> assertEquals(animal.getAnimal(), "Pato"),
                () -> assertEquals(animal.getSom(), "Quá Quá"));
    }
}