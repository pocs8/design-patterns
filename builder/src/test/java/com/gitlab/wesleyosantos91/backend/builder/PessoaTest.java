package com.gitlab.wesleyosantos91.backend.builder;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PessoaTest {

    private Pessoa pessoa;
    private final String NOME = "Wesley";
    private final String SOBRENOME = "Oliveira Santos";
    private final LocalDate DATA_NASCIMENTO = LocalDate.of(1991, 6, 12);


    @Test
    @DisplayName("Deve criar um objeto com builder")
    void name() {

        pessoa = new Pessoa.PessoaBuilder()
                    .nome(NOME)
                    .sobreNome(SOBRENOME)
                    .dataNascimento(DATA_NASCIMENTO)
                    .build();

        assertAll(() -> assertEquals(pessoa.getNome(), NOME),
                () -> assertEquals(pessoa.getSobreNome(), SOBRENOME),
                () -> assertEquals(pessoa.getDataNascimento(), DATA_NASCIMENTO));

    }
}