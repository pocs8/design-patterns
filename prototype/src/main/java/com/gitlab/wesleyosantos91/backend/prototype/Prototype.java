package com.gitlab.wesleyosantos91.backend.prototype;

public abstract class Prototype {

    public abstract Prototype copy();
}
