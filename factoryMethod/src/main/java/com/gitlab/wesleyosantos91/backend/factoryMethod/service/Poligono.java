package com.gitlab.wesleyosantos91.backend.factoryMethod.service;

public interface Poligono {
    Integer getNumeroDeLados();
}
