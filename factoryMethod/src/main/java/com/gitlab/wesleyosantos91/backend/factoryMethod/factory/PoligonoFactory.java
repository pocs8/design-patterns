package com.gitlab.wesleyosantos91.backend.factoryMethod.factory;

import com.gitlab.wesleyosantos91.backend.factoryMethod.enumeration.Lados;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.Poligono;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.impl.Petagono;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.impl.Quadrado;
import com.gitlab.wesleyosantos91.backend.factoryMethod.service.impl.Triangulo;

public class PoligonoFactory {

    private PoligonoFactory() {
    }

    public static Poligono getPoligono(Lados lados) {
        switch (lados) {
            case TRES:
                return new Triangulo();
            case QUATRO:
                return new Quadrado();
            case CINCO:
                return new Petagono();
            default:
                throw new IllegalArgumentException("Lado informado não encontrado");
        }
    }
}
